# Active Learning for Flower Classification

You can use every dataset that has an PyTorch's ImageFolder structure, and don't forget to modify the code according to your preferred configurations and datasets, but specifically this code is created for 5 Class Flower Classification.

This notebook's purpose is to know whether how active learning works using several methods: 

- Random Sampling
- Least Confidence Sampling
- Margin Confidence Sampling

## Little Warning
If you want to use this code, make sure on the Confidence Sampling methods you sort the samples descending. Otherwise, you won't get good performance because of the reversed sampling.

# Credit
This repository was made when I was an AI Engineer Intern at Nodeflux, kudos for the superb time there while being supervised by amazing mentors
